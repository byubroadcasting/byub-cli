#!/usr/bin/env node
const cdk = require('aws-cdk-lib')
const { EC2Stack } = require('@byub/cdk-lib/dist/lib/ec2-stack')
const DEFAULT = {
  REGION: 'us-east-1',
  VPC: 'vpc-07cf3592deefcc2d4',
  INSTANCETYPE: 't3.micro',
  INSTANCENAME: '',
  SECURITYGROUP: 'sg-0fc747c94950126ff',
  VOLUMESIZE: 32,
  IAMUSER: 'dev'
}

const app = new cdk.App()

const props = {
  vpc: app.node.tryGetContext('vpcToUse') ?? DEFAULT.VPC,
  instanceType: app.node.tryGetContext('instanceType') ?? DEFAULT.INSTANCETYPE,
  instanceName: app.node.tryGetContext('instanceName') ?? DEFAULT.INSTANCENAME,
  securityGroup: app.node.tryGetContext('securityGroupToUse') ?? DEFAULT.SECURITYGROUP,
  keyName: app.node.tryGetContext('keyName'), // no default, must be defined by cli
  volumeSize: app.node.tryGetContext('volumeSize') ?? DEFAULT.VOLUMESIZE,
  iamUserArn: app.node.tryGetContext('iamUserArn') ?? DEFAULT.IAMUSER,
  service: 'service-order',
  stage: 'dev',
  env: {
    account: process.env.CDK_DEFAULT_ACCOUNT,
    region: app.node.tryGetContext('region') ?? DEFAULT.REGION
  }
}

const username = props.iamUserArn.slice(31).replace('.', '-') || 'unknown'

// eslint-disable-next-line no-new
new EC2Stack(app, `ec2-instance-${username}`, props)
