# Introduction

This repository is a collection of CLI tools for software developers at BYU Broadcasting.

## Development

This project uses `yargs` to parse command line arguments. The entry point of
the CLI is in `bin/cli.js`. This is also the executable that gets invoked when
running `npx @byub/cli <command> [options]`.

The entry point for CLI commands is in `bin/cli.js`, which contains a simple
`yargs` configuration pointing to implementation logic in the `cmds/`
directory.

---

**Hint**: While doing local development, it's a common to want to see how new
functionality will work as if installed via NPM before actually publishing a
new package version. This is especially true for this project, since running
locally via `./bin/cli.js ...` can occasionally behave differently than via `npx
@byub/cli ...` or via the globally installed `byub` command.

A great way to do this is with `npm link`, which creates a symlink in the
global `node_modules` folder. See the
[documentation on npm-link](https://docs.npmjs.com/cli/v8/commands/npm-link)
for more info.

## Testing

Run `npm run test`.

## Usage

Run `npx @byub/cli --help` to see available commands.

## Installation

Using `npx`:
```bash
$ npx @byub/cli <command> [options]
```

You can also install the package globally and use `byub` (instead of `npx @byub/cli`):
```bash
$ npm i -g @byub/cli
$ byub <command> [options]
```

### Create an Event Schema for use with [Amazon EventBridge](https://aws.amazon.com/eventbridge/)

The following documentation describes various commands for creating an Event
Schema for use with Amazon EventBridge.

#### Generate [JSON-Schema](https://json-schema.org) for an Event

To create an event schema in JSON-schema format, run the following command:
```bash
npx @byub/cli event g MyFirstEvent
```

The generated JSON-schema is a skeleton for the event. After creating the event
schema, you should modify the schema to match the event you want to create.

For example, if the payload of the `MyFirstEvent` event should have a required
`name` field and an optional `age` field, then you should modify the schema as
follows:

```json
{
  "openapi": "3.0.0",
  "info": { ... },
  "paths": {},
  "components": {
    "schemas": {
      "AWSEvent": { ... },
      "MyFirstEvent": {
        "type": "object",
        "required": [
          "id",
          "name"
        ],
        "properties": {
          "id": {
            "type": "string"
          },
          "name": {
            "type": "string"
          },
          "age": {
            "type": "number"
          }
        }
      }
    }
  }
}
```

For more information on available data types and further schema customization,
see [JSON-Schema](https://json-schema.org).

#### Generate Standalone Cloudformation Template

To create a Cloudformation template (without Serverless Framework support) for
an EventSchema, run the following command:
```bash
npx @byub/cli events g MyFirstEvent --template=cloudformation
```

The generated template can be deployed as a stack to AWS CloudFormation.
```bash
~$ npx @byub/cli events g MyFirstEvent --template=cloudformation
~$ aws cloudformation create-stack --stack-name my-event-schema-stack --template-body file://MyFirstEvent.yml
```

The Cloudformation template contains three resources:
- `AWS::EventSchemas::Schema`: The schema itself.
- `AWS::Events::Rule`: A rule for EventBridge that routes events to a CloudWatch
  log group. This can be extremely useful for troubleshooting issues and
  inspecting the actual payload of events.
- `AWS::Logs::LogGroup`: The log group that the Event Rule will send events to.

#### Generate Cloudformation Template for Serverless Framework

To create a Cloudformation template that is compatible with [Serverless
Framework](https://serverless.com/framework/docs), run the following command:
```bash
npx @byub/cli events g MyFirstEvent --template=sls
```

The generated template is meant to be incorporated into a Serverless Framework
project. For example, the previous command would generate 
a template that looks something like the following:

```yaml
Resources:
  MyFirstEventSchema:
    Type: AWS::EventSchemas::Schema
    Properties:
      RegistryName: ${cf:regionbase.Platform5EventSchemaRegistryName}
      SchemaName: ${self:service}@MyFirstEvent
      Type: OpenApi3
      Content: |-
          <the JSON-schema for the event>

  MyFirstEventLogGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: /aws/events/${self:service}-MyFirstEvent-catchall
      RetentionInDays: 14

  MyFirstEventCatchAllRule:
    Type: AWS::Events::Rule
    Properties:
      Description: Catch all rule to send events to CloudWatch.
      EventBusName: ${cf:regionbase.BYUBEventBusName}
      Name: ${self:service}-MyFirstEvent-catchall
      Targets:
        - Arn:
            Fn::GetAtt: MyFirstEventLogGroup.Arn
          Id: ${self:service}-MyFirstEvent-catchall-rule
      EventPattern:
        source:
          - ${self:service}
        detail-type:
          - MyFirstEvent
```

After generating the template, simply copy/paste the Cloudformation resources to
the `resources.Resources` section of your `serverless.yml` file.

```yaml
service: service-abc

provider:
  name: aws
  # ...

functions:
  # ...

resources:
  Resources:
    MyFirstEventSchema:
      Type: AWS::EventSchemas::Schema
      Properties:
        # ...

    MyFirstEventLogGroup:
      Type: AWS::Logs::LogGroup
      Properties:
        # ...

    MyFirstEventCatchAllRule:
      Type: AWS::Events::Rule
      Properties:
        # ...
```

### Manage an EC2 Instance

The following documentation describes various commands for creating and managing AWS EC2 resources.

#### Deploy a new EC2 Instance

To create a basic instance run:
```bash
$ npx @byub/cli ec2 build -k my-key-pair
```
There are a number of options, which can be viewed by running:
```bash
$ npx @byub/cli ec2 build --help
```
If no options are specified, the tool defaults are:<br>
- Region: `us-east-1`
- VPC: `VPC for BYUB region us-east-1`
- Group: `us-east-1-public-securitygroup`
- Type: `t3.micro`
- Size: `32`

*Note: VPC and security group are specified by id, but the defaults here are idenitfied by name for readability.*

The `--key` (`-k`) option is required, and will either use the key pair with the same name in AWS,
or, if that key is not found, create a new key pair with the specified name. The private key will be
stored in `$HOMEDIR$/.ssh/<key-name>.pem`

**Warning:** Running the tool will terminate any instances deployed previously with this tool, with the
exception being if no paramaters have changed from the previous command.

#### Destroy the EC2 Instance

To terminate the instance, and tear down the CloudFormation stack, run:
```bash
$ npx @byub/cli ec2 destroy
```
Use the `--region` (`-r`) option if the instance is deployed in a region other than us-east-1.

#### List EC2 Instances

To get a list of instances in a region (defaults to us-east-1), run:
```bash
$ npx @byub/cli ec2 list -r us-east-1
```
Example output:
```bash
Name                 Id                  State  
an-ec2-instance      i-00123456789abcdef running
another-ec2-instance i-00abcdef123456789 running
my-ec2-instance      i-987654321facade00 stopped
```
The `--verbose` or `-v` tag will display (if available) the IPs, instance types, and key pairs of each instance.

#### Get the state of an EC2 Instance

To get the state of an instance, run:
```bash
$ npx @byub/cli ec2 state i-0112358beefcafe00
running
```
