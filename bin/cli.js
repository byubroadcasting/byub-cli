#!/usr/bin/env node

// eslint-disable-next-line no-unused-expressions
require('yargs/yargs')(process.argv.slice(2))
  .scriptName('@byub/cli')
  .commandDir('../cmds')
  .wrap(100)
  .demandCommand()
  .help('h')
  .alias('h', 'help')
  .example('$0 events --help')
  .example('$0 events g --help')
  .example('$0 co --help')
  .example('$0 co login')
  .example('$0 co add-scripts')
  .example('$0 version')
  .argv
