const os = require('os')
const fs = require('fs')
const { generateTemplate } = require('../cmds/event-cmds/event-templates')

describe('event-templates', () => {
  describe('#generateTemplate()', () => {
    it('should generate a JSON template', () => {
      const tmpDir = os.tmpdir()
      const eventName = 'test'

      // If outputPath is a folder, then it should create `test.json`
      generateTemplate({
        eventName,
        templateType: 'json',
        outputPath: tmpDir
      })

      expect(fs.existsSync(`${tmpDir}/${eventName}.json`)).toBe(true)

      // If outputPath is a filename, then it should create `TestEventSchema.json`
      generateTemplate({
        eventName,
        templateType: 'json',
        outputPath: `${tmpDir}/TestEventSchema.json`
      })

      expect(fs.existsSync(`${tmpDir}/TestEventSchema.json`)).toBe(true)
    })

    it('should generate a cloudformation template', () => {
      const tmpDir = os.tmpdir()
      const eventName = 'test'

      generateTemplate({
        eventName,
        templateType: 'cfn',
        outputPath: tmpDir
      })

      expect(fs.existsSync(`${tmpDir}/${eventName}.yml`)).toBe(true)
    })

    it('should generate a serverless template', () => {
      const tmpDir = os.tmpdir()
      const eventName = 'test'

      generateTemplate({
        eventName,
        templateType: 'sls',
        outputPath: tmpDir
      })

      expect(fs.existsSync(`${tmpDir}/${eventName}.yml`)).toBe(true)
    })
  })
})
