/**
 * @typedef {import('yargs').CommandModule} CommandModule
 */

/**
 * @type {CommandModule}
 */
module.exports = {
  command: 'events',
  desc: 'manage events',
  builder: yargs => yargs.commandDir('event-cmds'),
  handler: () => {}
}
