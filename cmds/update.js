/**
 * @typedef {import('yargs').CommandModule} CommandModule
 */
const { execSync } = require('child_process')
const { coLogin } = require('../constants.json')

/**
 * @type {CommandModule}
 */
module.exports = {
  command: 'update',
  desc: 'Update byub CLI to latest version. Note that this installs the latest version of the package globally.',
  builder: (yargs) => yargs.example('$0 update'),
  handler: (_argv) => {
    try {
      // authenticate with AWS CodeArtifact before installing
      execSync(coLogin, {
        stdio: 'inherit'
      })

      // install latest version of cli
      execSync('npm install -g @byub/cli@latest', {
        stdio: 'inherit'
      })

      const version = execSync('byub --version').toString().trim()
      console.log(`✅  Successfully updated @byub/cli to version ${version}`)
    } catch (error) {
      console.error(error)
    }
  }
}
