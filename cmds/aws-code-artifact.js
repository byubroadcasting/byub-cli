/**
 * @typedef {import('yargs').CommandModule} CommandModule
 */

/**
 * @type {CommandModule}
 */
module.exports = {
  command: 'co',
  desc: 'BYUB DMG specific tooling for AWS CodeArtifact',
  builder: yargs => yargs.commandDir('co-cmds'),
  handler: () => {}
}
