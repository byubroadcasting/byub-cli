const fs = require('fs')
const path = require('path')
const yaml = require('js-yaml')
const { CLOUDFORMATION_SCHEMA } = require('js-yaml-cloudformation-schema')

const eventBase = require('./templates/AWSEvent.template.json')

/**
 * @param {string} event - The event name
 * @returns {string}
 */
const _generateJSONSchemaTemplate = (event) => {
  const template = { ...eventBase }
  template.components.schemas.AWSEvent.properties.detail.$ref = template.components.schemas.AWSEvent.properties.detail.$ref.replace('__PLACEHOLDER__', event)
  template.components.schemas.AWSEvent.properties['detail-type'].default = event
  template.components.schemas[event] = {
    type: 'object',
    required: ['id'],
    properties: {
      id: {
        type: 'string'
      }
    }
  }

  return template
}

/**
 * @description Trim "Event" off end of eventName, if it exists
 * @param {string} eventName
 * @returns {string}
 */
const _generateResourcePrefix = eventName => eventName.replace(/Event$/i, '')

/**
 * @param {string} eventName
 */
const toJSONSchema = (eventName) => {
  const template = _generateJSONSchemaTemplate(eventName)
  return {
    fout: `${eventName}.json`,
    template: JSON.stringify(template, null, 2)
  }
}

/**
 * @param {string} event
 * @param {string} templateFileName
 * @returns {string}
 */
const toYAML = (event, templateFileName) => {
  const skeleton = yaml.load(fs.readFileSync(path.join(__dirname, './templates', templateFileName)), {
    schema: CLOUDFORMATION_SCHEMA
  })

  const eventSchema = _generateJSONSchemaTemplate(event)
  const resourcePrefix = _generateResourcePrefix(event)
  skeleton
    .Resources
    .__LOGICALID_PLACEHOLDER__EventSchema
    .Properties.Content = JSON.stringify(eventSchema, null, 2)

  skeleton
    .Resources
    .__LOGICALID_PLACEHOLDER__EventCatchAllRule
    .Properties
    .Targets[0]
    .Arn['Fn::GetAtt'] = '__LOGICALID_PLACEHOLDER__EventLogGroup.Arn'

  const template = yaml.dump(skeleton)
    .replace(/__(LOGICALID_)?PLACEHOLDER__/g, resourcePrefix)
    .replace(/__(EVENT_NAME_)?PLACEHOLDER__/g, event)
    .replace(/[\t ]*__NEWLINE__: ''[\t ]*/g, '')

  return {
    fout: `${resourcePrefix}.yml`,
    template
  }
}

/**
 * @param {string} eventName
 */
const toServerlessFramework = (eventName) => toYAML(eventName, 'serverlessjs.template.yml')

/**
 * @param {string} eventName
 */
const toCloudformation = (eventName) => toYAML(eventName, 'cloudformation.template.yml')

/**
 * @param {string} eventName
 * @param {string} templateType
 */
const generateTemplate = (eventName, templateType) => {
  switch (templateType) {
    case 'sls':
    case 'serverless':
      return toServerlessFramework(eventName)
    case 'cfn':
    case 'cloudformation':
      return toCloudformation(eventName)
    case 'json':
      return toJSONSchema(eventName)
    default:
      throw new Error(`Unknown template type: ${templateType}`)
  }
}

/**
 * @param {string} outputPathOrDir
 * @param {string} defaultOutputPath
 */
const resolveOutputPath = (outputPathOrDir, defaultOutputPath) => {
  if (!outputPathOrDir) {
    return path.join('./', defaultOutputPath)
  }

  const doesExist = fs.existsSync(outputPathOrDir)
  const isDir = doesExist && fs.lstatSync(outputPathOrDir).isDirectory()
  if (doesExist && isDir) {
    return path.join(outputPathOrDir, defaultOutputPath)
  }

  return path.resolve(outputPathOrDir)
}

/**
 * @param {Object} params
 * @param {string} params.eventName
 * @param {'sls' | 'serverless' | 'cloudformation' | 'cfn' | 'json'} params.templateType - The template type. `cfn` is an alias for `cloudformation` and `sls` is an alias for `serverless`.
 * @param {string=} params.outputPath
 */
exports.generateTemplate = ({ eventName, templateType, outputPath }) => {
  const { fout: defaultOutputPath, template } = generateTemplate(eventName, templateType)

  const fout = resolveOutputPath(outputPath, defaultOutputPath)
  fs.writeFileSync(fout, template)
  console.log(`✅ Event Schema for ${eventName} successfully saved at ${fout}`)
}
