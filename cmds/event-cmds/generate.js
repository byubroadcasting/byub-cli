/**
 * @typedef {import('yargs').CommandModule} CommandModule
 */

const { generateTemplate } = require('./event-templates')

const DEFAULT_TEMPLATE_TYPE = 'json'

/**
 * @type {CommandModule}
 */
module.exports = {
  command: ['g <event>', 'generate'],
  desc: 'Generate an event schema from template',
  builder: (yargs) => {
    yargs
      .positional('event', {
        describe: 'The name of the event',
        type: 'string'
      })
      .alias('output', 'o')
      .default('template', 'json')
      .option('output', {
        alias: 'o',
        desc: 'Filename of output. Defaults to "<event>.json".',
        type: 'string'
      })
      .option('template', {
        alias: 't',
        desc: 'The template used to generate the event schema.',
        choices: ['json', 'cloudformation', 'cfn', 'serverless', 'sls'],
        default: 'json'
      })
      .example('$0 events g UserSignedUpEvent --output=event.yml --template=sls')
      .example('$0 events g UserSignedUpEvent --output=event.yml --template=cloudformation')
      .example('$0 events g UserSignedUpEvent --output=event.json --template=json')
  },
  handler: argv => {
    try {
      generateTemplate({
        eventName: argv.event,
        templateType: argv.template ?? DEFAULT_TEMPLATE_TYPE,
        outputPath: argv.output ?? undefined
      })
    } catch (error) {
      console.error(error)
    }
  }
}
