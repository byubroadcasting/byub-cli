/**
 * @typedef {import('yargs').CommandModule} CommandModule
 */

/**
 * @type {CommandModule}
 */
module.exports = {
  command: 'ec2',
  builder: yargs => yargs
    .commandDir('ec2-cmds')
    .demandCommand(),
  handler: () => { }
}
