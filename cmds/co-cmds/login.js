/**
 * @typedef {import('yargs').CommandModule} CommandModule
 */
const { execSync } = require('child_process')
const { coLogin } = require('../../constants.json')

/**
  * @type {CommandModule}
  */
module.exports = {
  command: 'login',
  desc: 'Login to AWS CodeArtifact in order to access BYUB-DMG private NPM registry.',
  builder: (yargs) => {
    return yargs
      .example('$0 co login')
  },
  handler: (_argv) => {
    try {
      // See https://bitbucket.org/byubroadcasting/library-cdk/src/master/README.md for info on this command
      execSync(coLogin, {
        stdio: 'inherit'
      })
    } catch (error) {
      console.error(error)
    }
  }
}
