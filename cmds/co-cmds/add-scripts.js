/**
 * @typedef {import('yargs').CommandModule} CommandModule
 */
const fs = require('fs')
const path = require('path')

const { 'co:login': coLogin } = require('../../constants.json')

const preinstallScript = 'npm run co:login'

/**
 * @param {string} packageJsonPath - The full path to the `package.json` file to be updated.
 */
const updateScripts = (pkgPath) => {
  if (!fs.existsSync(pkgPath)) {
    return console.error(`Error: 'package.json' not found in '${pkgPath}'`)
  }

  try {
    const pkg = JSON.parse(fs.readFileSync(pkgPath))
    if (!pkg.scripts) {
      pkg.scripts = {}
    }

    if (!pkg.scripts.preinstall) {
      pkg.scripts.preinstall = preinstallScript
    } else if (!pkg.scripts.preinstall.includes(preinstallScript)) {
      pkg.scripts.preinstall = `${pkg.scripts.preinstall} && ${preinstallScript}`
    }

    pkg.scripts['co:login'] = coLogin

    fs.writeFileSync(pkgPath, JSON.stringify(pkg, null, 2))
    console.log(`✅ Succesfully added "co:login" script to ${pkgPath}`)
  } catch (error) {
    console.error(error)
    process.exit(1)
  }
}

/**
 * @type {CommandModule}
 */
module.exports = {
  command: ['add-scripts [path]', 'as'],
  desc: 'Adds scripts to `package.json`, allowing package with dependencies in private NPM registry to automatically authenticate with AWS CodeArtifact.',
  builder: (yargs) => {
    yargs
      .example('$0 co add-scripts /path/to/package.json')
      .example('$0 co as /path/to/package.json')
      .example('$0 co add-scripts .')
  },
  handler: (argv) => {
    /** @type {string} */
    let patharg = argv.path ?? '.'
    if (!patharg.endsWith('package.json')) {
      patharg = path.join(patharg, 'package.json')
    }

    patharg = path.resolve(patharg)
    updateScripts(patharg)
  }
}
