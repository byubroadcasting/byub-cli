/**
 * @typedef {import('yargs').CommandModule} CommandModule
 */
const AWS = require('aws-sdk')

/**
  * @type {CommandModule}
  */
module.exports = {
  command: 'state',
  desc: 'Get the state of an ec2 instance by id',
  builder: yargs => yargs
    .option('region', {
      alias: 'r',
      choices: ['us-east-1', 'us-east-2', 'us-west-2'],
      describe: 'specify the region to query, defaults to us-east-1',
      requiresArg: true
    })
    .option('verbose', {
      alias: 'v',
      type: 'count'
    })
    .demandCommand()
    .example('$0 ec2 list'),
  handler: async ({ region, verbose, id }) => {
    AWS.config.update({ region: region ?? 'us-east-1' })
    const ec2 = new AWS.EC2()
    const { Reservations: [{ Instances: [instance] }] } = await ec2.describeInstances({ InstanceIds: [id] }).promise()
    console.log(instance.State.Name)
    if (verbose >= 1) console.log(instance.PublicIpAddress ?? 'no IP')
    if (verbose >= 2) console.log(instance.KeyName ?? 'no key')
  }
}
