/**
 * @typedef {import('yargs').CommandModule} CommandModule
 */
const { execSync } = require('child_process')
const path = require('path')
const AWS = require('aws-sdk')
const prompts = require('prompts')
const fs = require('fs/promises')

const cdkPath = path.join(__dirname, '../..')
const sshPath = require('os').homedir() + '/.ssh'
const sleep = t => new Promise((resolve) => setTimeout(resolve, t))

/**
 * @type {CommandModule}
 */
module.exports = {
  command: 'build',
  desc: 'Deploy an aws cloud stack containing an ec2 instance.',
  builder: yargs => yargs
    .option('key', {
      alias: 'k',
      describe: 'provide name of ssh key pair in aws, creates a new key if none with specified name is found',
      demandOption: true,
      requiresArg: true
    })
    .option('region', {
      alias: 'r',
      choices: ['us-east-1', 'us-east-2', 'us-west-2'],
      describe: 'specify an availability region for the instance, default is us-east-1',
      requiresArg: true
    })
    .option('vpc', {
      alias: 'v',
      describe: 'specify a vpc for the instance by id',
      requiresArg: true
    })
    .option('group', {
      alias: 'g',
      describe: 'specify a security group for the instance by id',
      requiresArg: true
    })
    .option('type', {
      alias: 't',
      describe: 'specify the type of instance, default is t3.micro',
      requiresArg: true
    })
    .option('name', {
      alias: 'n',
      describe: 'specify a name for the instance',
      requiresArg: true
    })
    .option('size', {
      alias: 's',
      type: 'number',
      describe: 'specify the size in GB of the volume attached to the instance, default is 32',
      requiresArg: true
    })
    .option('connect', {
      alias: 'c',
      type: ['boolean', 'string'],
      describe: 'automatically connect to the instance; will look for a private key in ~/.ssh/<key-name>.pem if no path is provided',
      requiresArg: false
    })
    .help('h')
    .alias('h', 'help')
    .example('$0 ec2 build --key john-doe-key-pair --name john-db-access --region us-west-2 --vpc vpc-0123456789abcde --type t3.large --size 64 --connect ../my-key.pem')
    .example('$0 ec2 build -k john-doe-key-pair -t t2.nano -s 8 -c')
    .example('$0 ec2 build -k john-key'),
  handler: async ({ key, region, vpc, group, type, name, size, connect }) => {
    try {
      // Check if instance type is common, ask for confirmation if not
      const regex = /(t2|t3).(nano|micro|small|medium|large|xlarge)/
      if (type !== undefined && !regex.test(type)) {
        const confirmation = await prompts({
          type: 'confirm',
          name: 'typeConfirm',
          message: `Confirm deployment of instance of type ${type}?`,
          initial: false
        })
        if (!confirmation.typeConfirm) return
      }

      AWS.config.update({ region: region ?? 'us-east-1' })

      const sts = new AWS.STS()
      const callerId = await sts.getCallerIdentity().promise()

      // Check for key in cloud, create new if necessary
      const ec2 = new AWS.EC2()
      try {
        await ec2.describeKeyPairs({ KeyNames: [key] }).promise()
      } catch (err) {
        if (err.code === 'InvalidKeyPair.NotFound') {
          console.log('Key not found in EC2, creating new key...')
          const newKey = await ec2.createKeyPair({ KeyName: key }).promise()
          const keyFile = `${sshPath}/${newKey.KeyName}.pem`
          await fs.writeFile(keyFile, newKey.KeyMaterial, { mode: 400 })
          console.log(`Private key saved in ${keyFile}`)
        }
      }

      // Build context values for deployment
      const params = {
        iamUserArn: callerId.Arn,
        keyName: key,
        region: region,
        vpcToUse: vpc,
        instanceType: type,
        instanceName: name,
        securityGroupToUse: group,
        volumeSize: size
      }
      const args = Object.entries(params)
        .map(([key, value]) => {
          if (value === undefined) return ''
          else return ` -c ${key}=${value}`
        })
        .join('')

      execSync(`cdk deploy${args}`, {
        stdio: 'inherit',
        cwd: cdkPath
      })

      const instanceParams = {
        Filters: [{
          Name: 'tag:owner',
          Values: [callerId.Arn]
        },
        {
          Name: 'key-name',
          Values: [key]
        }]
      }

      // Check for stopped instance, restart if necessary
      let instanceId
      const checkState = async () => {
        const { Reservations: [{ Instances: [instance] }] } = await ec2.describeInstances(instanceParams).promise()
        instanceId = instance.InstanceId
        return instance.State.Name
      }
      if (await checkState() === 'stopped') console.log('Instance is stopped, attempting to restart...')
      while (await checkState() === 'stopped' || await checkState() === 'pending') {
        await sleep(1000)
        await ec2.startInstances({ InstanceIds: [instanceId] }).promise()
      }
      console.log(`Instance is now ${await checkState()}`)

      // Get ip of instance
      const { Reservations: [{ Instances: [{ PublicIpAddress }] }] } = await ec2.describeInstances(instanceParams).promise()
      if (connect) {
        let keyPath = `${sshPath}/${key}.pem`
        if (typeof connect === 'string') {
          keyPath = connect
        }
        if (PublicIpAddress) {
          console.log(`Connecting to ubuntu@${PublicIpAddress}...`)
          execSync(`ssh -o "IdentitiesOnly=yes" -i ${keyPath} ubuntu@${PublicIpAddress}`, {
            stdio: 'inherit'
          })
        } else console.log('Error getting ip address of instance')
      } else {
        if (PublicIpAddress) console.log(`Instance ip: ${PublicIpAddress}`)
      }
    } catch (error) {
      console.error(error)
    }
  }
}
