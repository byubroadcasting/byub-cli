/**
 * @typedef {import('yargs').CommandModule} CommandModule
 */
const AWS = require('aws-sdk')
const Table = require('cli-table')

/**
  * @type {CommandModule}
  */
module.exports = {
  command: 'list',
  desc: 'Get a list of ec2 instances in a region',
  builder: yargs => yargs
    .option('region', {
      alias: 'r',
      choices: ['us-east-1', 'us-east-2', 'us-west-2'],
      describe: 'specify the region to query, defaults to us-east-1',
      requiresArg: true
    })
    .option('verbose', {
      alias: 'v'
    })
    .example('$0 ec2 list'),
  handler: async ({ region, verbose }) => {
    AWS.config.update({ region: region ?? 'us-east-1' })
    const ec2 = new AWS.EC2()
    const { Reservations } = await ec2.describeInstances().promise()
    const table = new Table({
      chars: {
        // eslint-disable-next-line quote-props
        'top': '', 'top-mid': '', 'top-left': '', 'top-right': '', 'bottom': '', 'bottom-mid': '', 'bottom-left': '', 'bottom-right': '', 'left': '', 'left-mid': '', 'mid': '', 'mid-mid': '', 'right': '', 'right-mid': '', 'middle': ' '
      },
      style: { 'padding-left': 0, 'padding-right': 0 }
    })
    const headingRow = ['Name', 'Id', 'State']
    if (verbose) {
      headingRow.push(...['IP', 'Type', 'Key Pair'])
    }
    table.push(headingRow)
    Reservations.forEach(reservation => {
      reservation.Instances.forEach(instance => {
        const name = instance.Tags.find(tag => tag.Key === 'Name') ?? { Value: '---' }
        const dataRow = [name.Value, instance.InstanceId, instance.State.Name]
        if (verbose) {
          dataRow.push(...[instance.PublicIpAddress ?? '---', instance.InstanceType, instance.KeyName ?? '---'])
        }
        table.push(dataRow)
      })
    })
    console.log(table.toString())
  }
}
