/**
 * @typedef {import('yargs').CommandModule} CommandModule
 */
const { execSync } = require('child_process')
const path = require('path')
const cdkPath = path.join(__dirname, '../..')
const AWS = require('aws-sdk')
const sts = new AWS.STS()

/**
  * @type {CommandModule}
  */
module.exports = {
  command: 'destroy',
  desc: 'Destroy the stack containing the ec2 instance',
  builder: yargs => yargs
    .option('region', {
      alias: 'r',
      choices: ['us-east-1', 'us-east-2', 'us-west-2'],
      describe: 'specify the region of the stack to destroy, default is us-east-1',
      requiresArg: true
    })
    .example('$0 ec2 destroy'),
  handler: async ({ region }) => {
    try {
      const result = await sts.getCallerIdentity().promise()
      execSync(`cdk destroy -c iamUserArn=${result.Arn} -c region=${region}`, {
        stdio: 'inherit',
        cwd: cdkPath
      })
    } catch (error) {
      console.error(error)
    }
  }
}
