/**
 * @typedef {import('yargs').CommandModule} CommandModule
 */
const path = require('path')

/**
 * @type {CommandModule}
 */
module.exports = {
  command: 'version',
  desc: 'show current version',
  builder: (yargs) => yargs
    .example('$0 co login'),
  handler: () => {
    const { version } = require(path.join(__dirname, '../package.json'))
    console.log(version)
  }
}
